import com.google.gson.JsonObject;
import com.google.protobuf.Empty;
import com.tbyacoub.workflow.engine.grpc.WorkflowStepGrpc;
import com.tbyacoub.workflow.engine.grpc.WorkflowStepGrpc.*;
import com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.Arrays;
import java.util.List;

public class SampleClientApplication {

    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:6565")
                .usePlaintext()
                .build();

        WorkflowStepMessage workflowStepMessageCreateResponse = createWorkflowStep(channel);
        System.out.println(String.format("WorkflowStepMessageCreateResponse: %s", workflowStepMessageCreateResponse));

        WorkflowStepMessage workflowStepMessageFindResponse = findWorkflowStep(
                channel,
                workflowStepMessageCreateResponse
        );
        System.out.println(String.format("WorkflowStepMessageFindResponse: %s", workflowStepMessageFindResponse));

        WorkflowStepMessageList workflowStepMessageList = findAllById(channel);
        System.out.println(String.format(
                "findAllById: %s",
                workflowStepMessageList.getWorkflowStepMessageList()
        ));

        workflowStepMessageList = findAll(channel);
        System.out.println(String.format(
                "findAll: %s",
                workflowStepMessageList.getWorkflowStepMessageList()
        ));
    }

    private static WorkflowStepMessage createWorkflowStep(ManagedChannel channel) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("type", "CUSTOM");
        jsonObject.addProperty("expression", "expression");

        WorkflowStepBlockingStub workflowStepBlockingStub = WorkflowStepGrpc.newBlockingStub(channel);

        WorkflowStepMessage workflowStepModelRequest = WorkflowStepMessage.newBuilder()
                .setMetadata(jsonObject.toString()).build();

        return workflowStepBlockingStub.create(workflowStepModelRequest);
    }

    private static WorkflowStepMessage findWorkflowStep(
            ManagedChannel channel,
            WorkflowStepMessage workflowStepMessage
    ) {
        WorkflowStepBlockingStub workflowStepBlockingStub = WorkflowStepGrpc.newBlockingStub(channel);

        WorkflowStepQuery workflowStepQuery = WorkflowStepQuery
                .newBuilder()
                .setId(workflowStepMessage.getId())
                .build();

        return workflowStepBlockingStub.find(workflowStepQuery);
    }

    private static WorkflowStepMessageList findAllById(ManagedChannel channel) {
        WorkflowStepBlockingStub workflowStepBlockingStub = WorkflowStepGrpc.newBlockingStub(channel);

        List<Long> ids = Arrays.asList(2000L, 2005L, 2007L, 2008L, 2009L);

        FindAllByIdQuery findAllByIdQuery = FindAllByIdQuery
                .newBuilder()
                .addAllId(ids)
                .build();

        return workflowStepBlockingStub.findAllById(findAllByIdQuery);
    }

    private static WorkflowStepMessageList findAll(ManagedChannel channel) {
        WorkflowStepBlockingStub workflowStepBlockingStub = WorkflowStepGrpc.newBlockingStub(channel);
        Empty empty = Empty.newBuilder().build();
        return workflowStepBlockingStub.findAll(empty);
    }
}
