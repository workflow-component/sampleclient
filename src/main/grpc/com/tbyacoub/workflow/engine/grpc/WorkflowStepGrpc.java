package com.tbyacoub.workflow.engine.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.29.0)",
    comments = "Source: workflow_step.proto")
public final class WorkflowStepGrpc {

  private WorkflowStepGrpc() {}

  public static final String SERVICE_NAME = "WorkflowStep";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage,
      com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> getCreateMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "create",
      requestType = com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage.class,
      responseType = com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage,
      com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> getCreateMethod() {
    io.grpc.MethodDescriptor<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage, com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> getCreateMethod;
    if ((getCreateMethod = WorkflowStepGrpc.getCreateMethod) == null) {
      synchronized (WorkflowStepGrpc.class) {
        if ((getCreateMethod = WorkflowStepGrpc.getCreateMethod) == null) {
          WorkflowStepGrpc.getCreateMethod = getCreateMethod =
              io.grpc.MethodDescriptor.<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage, com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "create"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage.getDefaultInstance()))
              .setSchemaDescriptor(new WorkflowStepMethodDescriptorSupplier("create"))
              .build();
        }
      }
    }
    return getCreateMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery,
      com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> getFindMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "find",
      requestType = com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery.class,
      responseType = com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery,
      com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> getFindMethod() {
    io.grpc.MethodDescriptor<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery, com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> getFindMethod;
    if ((getFindMethod = WorkflowStepGrpc.getFindMethod) == null) {
      synchronized (WorkflowStepGrpc.class) {
        if ((getFindMethod = WorkflowStepGrpc.getFindMethod) == null) {
          WorkflowStepGrpc.getFindMethod = getFindMethod =
              io.grpc.MethodDescriptor.<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery, com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "find"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage.getDefaultInstance()))
              .setSchemaDescriptor(new WorkflowStepMethodDescriptorSupplier("find"))
              .build();
        }
      }
    }
    return getFindMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> getFindAllMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findAll",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> getFindAllMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> getFindAllMethod;
    if ((getFindAllMethod = WorkflowStepGrpc.getFindAllMethod) == null) {
      synchronized (WorkflowStepGrpc.class) {
        if ((getFindAllMethod = WorkflowStepGrpc.getFindAllMethod) == null) {
          WorkflowStepGrpc.getFindAllMethod = getFindAllMethod =
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findAll"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList.getDefaultInstance()))
              .setSchemaDescriptor(new WorkflowStepMethodDescriptorSupplier("findAll"))
              .build();
        }
      }
    }
    return getFindAllMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery,
      com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> getFindAllByIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findAllById",
      requestType = com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery.class,
      responseType = com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery,
      com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> getFindAllByIdMethod() {
    io.grpc.MethodDescriptor<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery, com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> getFindAllByIdMethod;
    if ((getFindAllByIdMethod = WorkflowStepGrpc.getFindAllByIdMethod) == null) {
      synchronized (WorkflowStepGrpc.class) {
        if ((getFindAllByIdMethod = WorkflowStepGrpc.getFindAllByIdMethod) == null) {
          WorkflowStepGrpc.getFindAllByIdMethod = getFindAllByIdMethod =
              io.grpc.MethodDescriptor.<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery, com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findAllById"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList.getDefaultInstance()))
              .setSchemaDescriptor(new WorkflowStepMethodDescriptorSupplier("findAllById"))
              .build();
        }
      }
    }
    return getFindAllByIdMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static WorkflowStepStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<WorkflowStepStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<WorkflowStepStub>() {
        @java.lang.Override
        public WorkflowStepStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new WorkflowStepStub(channel, callOptions);
        }
      };
    return WorkflowStepStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static WorkflowStepBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<WorkflowStepBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<WorkflowStepBlockingStub>() {
        @java.lang.Override
        public WorkflowStepBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new WorkflowStepBlockingStub(channel, callOptions);
        }
      };
    return WorkflowStepBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static WorkflowStepFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<WorkflowStepFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<WorkflowStepFutureStub>() {
        @java.lang.Override
        public WorkflowStepFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new WorkflowStepFutureStub(channel, callOptions);
        }
      };
    return WorkflowStepFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class WorkflowStepImplBase implements io.grpc.BindableService {

    /**
     */
    public void create(com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage request,
        io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> responseObserver) {
      asyncUnimplementedUnaryCall(getCreateMethod(), responseObserver);
    }

    /**
     */
    public void find(com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery request,
        io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> responseObserver) {
      asyncUnimplementedUnaryCall(getFindMethod(), responseObserver);
    }

    /**
     */
    public void findAll(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> responseObserver) {
      asyncUnimplementedUnaryCall(getFindAllMethod(), responseObserver);
    }

    /**
     */
    public void findAllById(com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery request,
        io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> responseObserver) {
      asyncUnimplementedUnaryCall(getFindAllByIdMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getCreateMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage,
                com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage>(
                  this, METHODID_CREATE)))
          .addMethod(
            getFindMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery,
                com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage>(
                  this, METHODID_FIND)))
          .addMethod(
            getFindAllMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList>(
                  this, METHODID_FIND_ALL)))
          .addMethod(
            getFindAllByIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery,
                com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList>(
                  this, METHODID_FIND_ALL_BY_ID)))
          .build();
    }
  }

  /**
   */
  public static final class WorkflowStepStub extends io.grpc.stub.AbstractAsyncStub<WorkflowStepStub> {
    private WorkflowStepStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected WorkflowStepStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new WorkflowStepStub(channel, callOptions);
    }

    /**
     */
    public void create(com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage request,
        io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCreateMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void find(com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery request,
        io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getFindMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void findAll(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getFindAllMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void findAllById(com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery request,
        io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getFindAllByIdMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class WorkflowStepBlockingStub extends io.grpc.stub.AbstractBlockingStub<WorkflowStepBlockingStub> {
    private WorkflowStepBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected WorkflowStepBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new WorkflowStepBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage create(com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage request) {
      return blockingUnaryCall(
          getChannel(), getCreateMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage find(com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery request) {
      return blockingUnaryCall(
          getChannel(), getFindMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList findAll(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getFindAllMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList findAllById(com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery request) {
      return blockingUnaryCall(
          getChannel(), getFindAllByIdMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class WorkflowStepFutureStub extends io.grpc.stub.AbstractFutureStub<WorkflowStepFutureStub> {
    private WorkflowStepFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected WorkflowStepFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new WorkflowStepFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> create(
        com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage request) {
      return futureUnaryCall(
          getChannel().newCall(getCreateMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage> find(
        com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery request) {
      return futureUnaryCall(
          getChannel().newCall(getFindMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> findAll(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getFindAllMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList> findAllById(
        com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery request) {
      return futureUnaryCall(
          getChannel().newCall(getFindAllByIdMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_CREATE = 0;
  private static final int METHODID_FIND = 1;
  private static final int METHODID_FIND_ALL = 2;
  private static final int METHODID_FIND_ALL_BY_ID = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final WorkflowStepImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(WorkflowStepImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CREATE:
          serviceImpl.create((com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage) request,
              (io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage>) responseObserver);
          break;
        case METHODID_FIND:
          serviceImpl.find((com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepQuery) request,
              (io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessage>) responseObserver);
          break;
        case METHODID_FIND_ALL:
          serviceImpl.findAll((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList>) responseObserver);
          break;
        case METHODID_FIND_ALL_BY_ID:
          serviceImpl.findAllById((com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.FindAllByIdQuery) request,
              (io.grpc.stub.StreamObserver<com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.WorkflowStepMessageList>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class WorkflowStepBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    WorkflowStepBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.tbyacoub.workflow.engine.grpc.WorkflowStepOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("WorkflowStep");
    }
  }

  private static final class WorkflowStepFileDescriptorSupplier
      extends WorkflowStepBaseDescriptorSupplier {
    WorkflowStepFileDescriptorSupplier() {}
  }

  private static final class WorkflowStepMethodDescriptorSupplier
      extends WorkflowStepBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    WorkflowStepMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (WorkflowStepGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new WorkflowStepFileDescriptorSupplier())
              .addMethod(getCreateMethod())
              .addMethod(getFindMethod())
              .addMethod(getFindAllMethod())
              .addMethod(getFindAllByIdMethod())
              .build();
        }
      }
    }
    return result;
  }
}
